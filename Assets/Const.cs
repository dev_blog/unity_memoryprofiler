﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/

public static class Const
{
    public const string Res1 = "Res1";
    public const string Res2 = "Res2";
}