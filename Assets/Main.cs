﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/

using System;
using UnityEngine;

public class Main : MonoBehaviour {

	void Start ()
	{
		var bundle1Bytes = Resources.Load<TextAsset>("bundle1").bytes;
		var bundle2Bytes = Resources.Load<TextAsset>("bundle2").bytes;
		var assetBundle1 = AssetBundle.LoadFromMemory(bundle1Bytes);
		var assetBundle2 = AssetBundle.LoadFromMemory(bundle2Bytes);

		var res1 = assetBundle1.LoadAsset<GameObject>(Const.Res1);
		var res2 = assetBundle2.LoadAsset<GameObject>(Const.Res2);

		Instantiate(res1);
		Instantiate(res2);
	}
	
}
