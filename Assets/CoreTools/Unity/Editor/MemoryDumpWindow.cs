﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/

using System.IO;
using UnityEditor;
using UnityEditor.MemoryProfiler;
using UnityEngine;

namespace CoreTools.Unity.Editor
{
    public class MemoryDumpWindow : EditorWindow
    {
        [MenuItem("Window/MemoryDump")]
        static void Create()
        {
            EditorWindow.GetWindow<MemoryDumpWindow>();
        }

        private const string FileLog = "Memory.Log";
        private const string FileLogSummary = "MemorySummary.Log";
        private const string DuplicateReport = "Duplicate.Log";

        #region -State

        private float memoryCap;

        #endregion

        #region -Lifecycle

        private void Awake()
        {
            MemorySnapshot.OnSnapshotReceived += OnMemorySnapshotReceived;
            titleContent = new GUIContent("Memory dump");
        }

        public void OnDestroy()
        {
            MemorySnapshot.OnSnapshotReceived -= OnMemorySnapshotReceived;
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.LabelField("Min memory category size:");
                memoryCap = EditorGUILayout.Slider(memoryCap, 0f, 5f);
                EditorGUILayout.LabelField(string.Format("Memory log will be stored to: {0}", FileLog));
                EditorGUILayout.LabelField(string.Format("Memory log summary will be stored to: {0}", FileLogSummary));
                
                if (GUILayout.Button("Request memory dump"))
                {
                    MemorySnapshot.RequestNewSnapshot();
                }          
            }
            EditorGUILayout.EndVertical();
        }


        #endregion
        

        private void OnMemorySnapshotReceived(PackedMemorySnapshot memorySnapshot)
        {
            var memoryCapBytes = (uint) (memoryCap * 1024 * 1024);
            var processedSnapshot = MemoryDump.ProcessMemorySnapshot(memorySnapshot);
            var dumpReport = MemoryDumpReports.MakeSizeReport(processedSnapshot, memoryCapBytes );
            File.WriteAllText(FileLog, dumpReport);

            var summaryReport = MemoryDumpReports.MakeSummaryReport(processedSnapshot, memoryCapBytes);
            File.WriteAllText(FileLogSummary, summaryReport);
            
            var duplicateReport = MemoryDumpReports.MakeDuplicatesReport(processedSnapshot);
            File.WriteAllText(DuplicateReport, duplicateReport);
        }
    }
}