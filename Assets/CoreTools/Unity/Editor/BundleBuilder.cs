﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/

using UnityEditor;

namespace CoreTools.Unity.Editor
{
    public static class BundleBuilder
    {
        [MenuItem("Tools/BuildBundles")]
        public static void BuildBundles()
        {
            var bundle1 = new AssetBundleBuild
            {
                assetBundleName = "Bundle1.bytes",
                addressableNames = new [] { Const.Res1 },
                assetNames = new [] {"Assets/Art/Sprite1.prefab"}
            };
            
            var bundle2 = new AssetBundleBuild
            {
                assetBundleName = "Bundle2.bytes",
                addressableNames = new [] { Const.Res2 },
                assetNames = new [] {"Assets/Art/Sprite2.prefab"}
            };
            
            var bundles = new[] { bundle1, bundle2 };
            var bunldeOptions = BuildAssetBundleOptions.StrictMode | 
                                BuildAssetBundleOptions.DeterministicAssetBundle | 
                                BuildAssetBundleOptions.DisableLoadAssetByFileName | 
                                BuildAssetBundleOptions.DisableLoadAssetByFileNameWithExtension;
            
            var platform = EditorUserBuildSettings.activeBuildTarget;
            string outputpath = "Assets/Resources";
            BuildPipeline.BuildAssetBundles( outputpath, bundles, bunldeOptions, platform);
            
        }
    }
}