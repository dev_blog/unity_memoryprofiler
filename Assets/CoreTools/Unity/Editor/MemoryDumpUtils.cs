﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/

using System;
using System.Collections.Generic;
using UnityEditor.MemoryProfiler;

namespace CoreTools.Unity.Editor
{
    public static class MemoryDumpUtils
    {
        public static string GetFormatedSizeString(ulong size)
        {
            if (size > 1024 * 1024)
            {
                return (size / 1024 / 1024) + " Mb";
            }
		
            if (size > 1024)
            {
                return (size / 1024) + " kb";
            }

            return size + " bytes";
        }

        private const int STRING_LENGTH_FIELD = 1;
        private const int BYTES_PER_CHAR = 2;
        private const int TERMINATOR_SIZE = 2;
        private const string TYPE_NAME = "System.String";
        
        private static int ReadStringObjectSizeInBytes(MemoryDump.BytesAndOffset bo, int objectHeaderSize)
        {
            var lengthPointer = bo.Add(objectHeaderSize);
            var length = lengthPointer.ReadInt32();

            return objectHeaderSize + STRING_LENGTH_FIELD + (length * BYTES_PER_CHAR) + TERMINATOR_SIZE;
        }
        
        public static int SizeOfObjectInBytes(TypeDescription typeDescription, MemoryDump.BytesAndOffset bo, 
                                              ulong address, PackedMemorySnapshot snapshot)
        {
            if (typeDescription.isArray)
            {
                return ReadArrayObjectSizeInBytes(snapshot.managedHeapSections, address, 
                    typeDescription, snapshot.typeDescriptions,
                    snapshot.virtualMachineInformation);
            }

            if (typeDescription.name == TYPE_NAME)
            {
                return ReadStringObjectSizeInBytes(bo, snapshot.virtualMachineInformation.objectHeaderSize);
            }

            return typeDescription.size;
        }
        
        public static TypeDescription GetTypeDescription(MemorySection[] heap, ulong objectAddress,		
            VirtualMachineInformation virtualMachineInformation, Dictionary<ulong, TypeDescription> typeInfoToTypeDescription)
        {
            TypeDescription typeDescription;

            // IL2CPP has the class pointer as the first member of the object.
            if (!typeInfoToTypeDescription.TryGetValue(objectAddress, out typeDescription))
            {
                // Mono has a vtable pointer as the first member of the object.
                // The first member of the vtable is the class pointer.
                var typePointerLocation = FindPointerLocation(heap, objectAddress, virtualMachineInformation);
                var typeAdress = typePointerLocation.ReadPointer();
                typeDescription = typeInfoToTypeDescription[typeAdress];
            }

            return typeDescription;
        }
        
        public static int ReadArrayObjectSizeInBytes(MemorySection[] heap, UInt64 address,
            TypeDescription arrayType, TypeDescription[] typeDescriptions,
            VirtualMachineInformation virtualMachineInformation)
        {
            var arrayLength = ReadArrayLength(heap, address, arrayType, virtualMachineInformation);
            var elementType = typeDescriptions[arrayType.baseOrElementTypeIndex];
            var elementSize = elementType.isValueType ? elementType.size : virtualMachineInformation.pointerSize;
            return virtualMachineInformation.arrayHeaderSize + elementSize * arrayLength;
        }
        
        private static int ReadArrayLength(MemorySection[] heap, UInt64 address, TypeDescription arrayType,
            VirtualMachineInformation virtualMachineInformation)
        {
            var bo = FindPointerLocation(heap, address, virtualMachineInformation);

            var bounds = bo.Add(virtualMachineInformation.arrayBoundsOffsetInHeader).ReadPointer();

            if (bounds == 0)
            {
                return (int) bo.Add(virtualMachineInformation.arraySizeOffsetInHeader).ReadPointer();
            }

            var cursor = FindPointerLocation(heap, bounds, virtualMachineInformation);
            int length = 1;
            for (int i = 0; i != arrayType.arrayRank; i++)
            {
                length *= (int) cursor.ReadPointer();
                cursor = cursor.Add(virtualMachineInformation.pointerSize == 4 ? 8 : 16);
            }

            return length;
        }
        
        //Make Find a Binary tree search and Profile
        public static MemoryDump.BytesAndOffset FindPointerLocation(MemorySection[] heap, UInt64 address,
            VirtualMachineInformation virtualMachineInformation)
        {
            foreach (var segment in heap)
            {
                var segmentEndAdress = segment.startAddress + (ulong) segment.bytes.Length;
                if (address >= segment.startAddress && address < segmentEndAdress)
                {
                    return new MemoryDump.BytesAndOffset()
                    {
                        bytes = segment.bytes,
                        offset = (int) (address - segment.startAddress),
                        pointerSize = virtualMachineInformation.pointerSize
                    };
                }
            }

            return new MemoryDump.BytesAndOffset();
        }
    }
}