﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/

using System.Collections.Generic;

namespace CoreTools.Unity.Editor
{
    public class UlongComparer : IEqualityComparer<ulong>
    {
        public bool Equals(ulong x, ulong y)
        {
            return x == y;
        }

        public int GetHashCode(ulong obj)
        {
            return (int) obj;
        }
    }
}