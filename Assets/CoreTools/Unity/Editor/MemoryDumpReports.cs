﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UnityEditor.MemoryProfiler;

namespace CoreTools.Unity.Editor
{
    public static class MemoryDumpReports
    {
        private static NumberFormatInfo nfi;
        
        static MemoryDumpReports()
        {
            nfi = new CultureInfo("en-US", false).NumberFormat;
            nfi.CurrencyDecimalSeparator = ".";
        }

        public static string MakeSummaryReport(MemoryDump.ProcessedMemorySnapshot snapshot, uint categoryMemoryCap)
        {
            var sb = new StringBuilder();
            var managedTypesToSize = new Dictionary<TypeDescription, ulong>(snapshot.ManagedTypes.Length);
            for (var i = 0; i < snapshot.ManagedTypes.Length; ++i)
            {
                managedTypesToSize.Add(snapshot.ManagedTypes[i], snapshot.ManagedTypeToSize[i]);
            }

            sb.AppendLine("Managed types:");
            sb.AppendLine();
            
            var managedTypesSorted = snapshot.ManagedTypes.OrderByDescending( x => managedTypesToSize[x]).ToArray();
            for (var i = 0; i < managedTypesSorted.Length; ++i)
            {
                var className = managedTypesSorted[i].name;
                var size = managedTypesToSize[managedTypesSorted[i]];
                if (size > categoryMemoryCap)
                {
                    sb.AppendLine(className + " : " + MemoryDumpUtils.GetFormatedSizeString(size));
                }
            }
 
            var nativeTypeToSize = new Dictionary<PackedNativeType, ulong>(snapshot.NativeTypes.Length);
            for (var i = 0; i < snapshot.NativeTypes.Length; ++i)
            {
                nativeTypeToSize.Add(snapshot.NativeTypes[i], snapshot.NativeTypeToSize[i]);
            }

            sb.AppendLine();
            sb.AppendLine("Native types:");
            sb.AppendLine();
            
            var nativeTypesSorted = snapshot.NativeTypes.OrderByDescending( x => nativeTypeToSize[x]).ToArray();
            for (var i = 0; i < nativeTypesSorted.Length; ++i)
            {
                var className = nativeTypesSorted[i].name;
                var size = nativeTypeToSize[nativeTypesSorted[i]];
                if (size > categoryMemoryCap)
                {
                    sb.AppendLine(className + " : " + MemoryDumpUtils.GetFormatedSizeString(size));
                }
            }
            

            sb.AppendLine();
            sb.AppendLine("Native memory used " + MemoryDumpUtils.GetFormatedSizeString(snapshot.NativeMemoryUsed));
            
            if (snapshot.ManagedHeapSections.Length > 0)
            {
                PrintHeapSize(sb, snapshot);
            }
            return sb.ToString();
        }
        
        public static string MakeSizeReport(MemoryDump.ProcessedMemorySnapshot snapshot, uint categoryMemoryCap)
        {
            var sb = new StringBuilder();

            sb.AppendLine("Managed objects: ");
            sb.AppendLine();
            
            for ( var i = 0; i < snapshot.ManagedTypes.Length; ++i)
            {
                var managedType = snapshot.ManagedTypes[i];
                var list = snapshot.TypeToManagedObjects[i];
                if (list != null && snapshot.ManagedTypeToSize[i] > categoryMemoryCap)
                {
                    sb.AppendLine(managedType.name + " : " + MemoryDumpUtils.GetFormatedSizeString(snapshot.ManagedTypeToSize[i]));
                    var orderedList = list.OrderByDescending(x => x.Size);
                    foreach (var item in orderedList)
                    {
                        sb.AppendLine(item.TypeDescription.name + " " + MemoryDumpUtils.GetFormatedSizeString((ulong) item.Size));
                    }
                    
                    sb.AppendLine();
                }
            }

            sb.AppendLine();

            sb.AppendLine("Native objects: ");
            sb.AppendLine();
            
            for (var i = 0; i < snapshot.NativeTypes.Length; ++i)
            {
                var className = snapshot.NativeTypes[i].name;
                var list = snapshot.TypeToNativeObjects[i];
                if (list != null && snapshot.NativeTypeToSize[i] > categoryMemoryCap)
                {
                    sb.AppendLine(className + " : " + MemoryDumpUtils.GetFormatedSizeString(snapshot.NativeTypeToSize[i]));
                    var orderedList = list.OrderByDescending(x => x.Size);
                    foreach (var item in orderedList)
                    {
                        sb.AppendLine(item.Name + " " + MemoryDumpUtils.GetFormatedSizeString((ulong) item.Size));
                    }

                    sb.AppendLine();
                }

            }

            if (snapshot.ManagedHeapSections.Length > 0)
            {
                PrintHeapSize(sb, snapshot);
            }

            sb.AppendLine("Native memory used " + MemoryDumpUtils.GetFormatedSizeString(snapshot.NativeMemoryUsed));

            return sb.ToString();
        }


        class DuplicateRecord
        {
            public int Times;
            public int Size;
            public string Name;
        }

        public static string MakeDuplicatesReport(MemoryDump.ProcessedMemorySnapshot snapshot)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < snapshot.NativeTypes.Length; ++i)
            {
                var className = snapshot.NativeTypes[i].name;
                var list = snapshot.TypeToNativeObjects[i];
                if (list != null )
                {
                    var dict = new Dictionary<string, DuplicateRecord>();
                    var duplicationHappened = false;
                    foreach (var item in list)
                    {     
                        var itemId = item.Name + item.Size;
                        DuplicateRecord record = null;
                        if (!dict.TryGetValue(itemId, out record))
                        {
                            record = dict[itemId] = new DuplicateRecord();
                            record.Name = item.Name;
                            record.Size = item.Size;
                        }

                        record.Times++;
                        duplicationHappened |= record.Times > 1;
                    }

                    if (duplicationHappened)
                    {
                        sb.AppendLine(className + ": ");
                        
                        foreach (var item in dict)
                        {
                            if (item.Value.Times > 1)
                            {
                                var msg = "Item '{0}' of size {1} is duplicated {2} times";
                                sb.AppendLine(string.Format(msg, item.Value.Name, 
                                    MemoryDumpUtils.GetFormatedSizeString( (ulong) item.Value.Size), item.Value.Times));
                            }
                        }

                        sb.AppendLine();
                    }
                }
            }

            return sb.ToString();
        }

        private static void PrintHeapSize(StringBuilder sb, MemoryDump.ProcessedMemorySnapshot snapshot)
        {
            var managedHeapsOverallSize = (ulong) snapshot.ManagedHeapSections.Sum(x => x.bytes.Length);
                
            sb.AppendLine();
            sb.AppendLine("Heap size " + MemoryDumpUtils.GetFormatedSizeString(managedHeapsOverallSize));
        }
    }
}