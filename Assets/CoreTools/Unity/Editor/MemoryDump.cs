﻿/*
* Copyright (c) 2018 Anton Trukhan
* This file is subject to the terms and conditions defined in
* file 'README.md', which is part of this source code package.
*/

using System;
using System.Collections.Generic;
using UnityEditor.MemoryProfiler;

namespace CoreTools.Unity.Editor
{
	public static class MemoryDump
	{
		
		public struct BytesAndOffset
		{
			public byte[] bytes;
			public int offset;
			public int pointerSize;

			public UInt64 ReadPointer()
			{
				if (pointerSize == 4)
					return BitConverter.ToUInt32(bytes, offset);
				if (pointerSize == 8)
					return BitConverter.ToUInt64(bytes, offset);
				throw new ArgumentException("Unexpected pointersize: " + pointerSize);
			}

			public Int32 ReadInt32()
			{
				return BitConverter.ToInt32(bytes, offset);
			}

			public BytesAndOffset Add(int add)
			{
				return new BytesAndOffset() {bytes = bytes, offset = offset + add, pointerSize = pointerSize};
			}
		}

		public class ManagedObjectInfo
		{
			public TypeDescription TypeDescription;
			public int Size;
		}

		public class NativeObjectInfo
		{
			public int Size;
			public string Name;
		}

		public class ProcessedMemorySnapshot
		{
			//NativeObjects
			public List<NativeObjectInfo>[] TypeToNativeObjects;
			public ulong[] NativeTypeToSize;
			public PackedNativeType[] NativeTypes;
			public ulong NativeMemoryUsed;
			
			//ManagedObjects
			public List<ManagedObjectInfo>[] TypeToManagedObjects;
			public ulong[] ManagedTypeToSize;
			public TypeDescription[] ManagedTypes;
			
			
			public MemorySection[] ManagedHeapSections;
		}


		public static ProcessedMemorySnapshot ProcessMemorySnapshot(PackedMemorySnapshot memorySnapshot)
		{
			var snapshot = new ProcessedMemorySnapshot();
			var typeInfoToTypeDescription = new Dictionary<ulong, TypeDescription>(
				memorySnapshot.typeDescriptions.Length, new UlongComparer());

			foreach (var item in memorySnapshot.typeDescriptions)
			{
				typeInfoToTypeDescription.Add(item.typeInfoAddress, item);
			}

			ProcessManagedObjects(snapshot, memorySnapshot, typeInfoToTypeDescription);

			snapshot.ManagedHeapSections = memorySnapshot.managedHeapSections;

			ProcessNativeObjects(snapshot, memorySnapshot);

			return snapshot;
		}

		private static void ProcessManagedObjects(ProcessedMemorySnapshot snapshot, 
												  PackedMemorySnapshot memorySnapshot,
												  Dictionary<ulong, TypeDescription> typeInfoToTypeDescription)
		{
			snapshot.TypeToManagedObjects = new List<ManagedObjectInfo>[memorySnapshot.typeDescriptions.Length];
			snapshot.ManagedTypeToSize = new ulong[memorySnapshot.typeDescriptions.Length];
			snapshot.ManagedTypes = memorySnapshot.typeDescriptions;
			
			for (var i = 0; i < memorySnapshot.gcHandles.Length; ++i)
			{
				var gcHandle = memorySnapshot.gcHandles[i];
				var originalHeapAddress = gcHandle.target;
				var pointerLocation = MemoryDumpUtils.FindPointerLocation(memorySnapshot.managedHeapSections, originalHeapAddress,
					memorySnapshot.virtualMachineInformation);

				var managedHeapPointer = pointerLocation.ReadPointer();
				TypeDescription typeDescription = MemoryDumpUtils.GetTypeDescription(memorySnapshot.managedHeapSections,
					managedHeapPointer, memorySnapshot.virtualMachineInformation, typeInfoToTypeDescription);

				int size = MemoryDumpUtils.SizeOfObjectInBytes(typeDescription, pointerLocation, 
															   originalHeapAddress, memorySnapshot);

				var classId = typeDescription.typeIndex;
				var objectsList = snapshot.TypeToManagedObjects[classId] ?? 
				                  (snapshot.TypeToManagedObjects[classId] = new List<ManagedObjectInfo>());
				
				objectsList.Add(new ManagedObjectInfo {TypeDescription = typeDescription, Size = size});
				snapshot.ManagedTypeToSize[typeDescription.typeIndex] += (uint) size;
			}
		}

		private static void ProcessNativeObjects(ProcessedMemorySnapshot snapshot, PackedMemorySnapshot memorySnapshot)
		{
			snapshot.TypeToNativeObjects = new List<NativeObjectInfo>[memorySnapshot.nativeTypes.Length];
			snapshot.NativeTypeToSize = new ulong[memorySnapshot.nativeTypes.Length];
			snapshot.NativeTypes = memorySnapshot.nativeTypes;
			
			for (var i = 0; i < memorySnapshot.nativeObjects.Length; ++i)
			{
				var nativeObject = memorySnapshot.nativeObjects[i];
				var classId = nativeObject.nativeTypeArrayIndex;

				var objectsList = snapshot.TypeToNativeObjects[classId] ?? 
				                  (snapshot.TypeToNativeObjects[classId] = new List<NativeObjectInfo>());

				objectsList.Add(new NativeObjectInfo
				{
					Name = nativeObject.name,
					Size = nativeObject.size,
				});
				snapshot.NativeMemoryUsed += (uint) nativeObject.size;
				snapshot.NativeTypeToSize[classId] += (uint) nativeObject.size;
			}
		}
	}
}
